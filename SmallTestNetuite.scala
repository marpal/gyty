object SmallTestNetsuite {

  def startsWithCapital(word: String): Boolean = {
    word.charAt(0) == word.charAt(0).toUpper
  }

  def isNumeric(word: String): Boolean = {
    word.forall(Character.isDigit)
  }

  def reverseList[T](list: List[T]): List[T] = {
    val newList = list match {
      case Nil => Nil
      case x :: xs => reverseList(xs) ::: List(x)
    }

    return newList

  }

  def sumAllNumbers(numbers: List[Double]): Double = {
    numbers.foldLeft(0.0)(_ + _)
  }

  def multiplyAllNumbers(numbers: List[Double]): Double = {
    numbers.foldLeft(1.0)(_ * _)
  }
  
  class SwapHelper() {
    var number1 = 0
    var number2 = 0
  }

  def swapWithoutNewVariable(swapHelper:SwapHelper): Unit = {
    swapHelper.number1 = swapHelper.number1 ^ swapHelper.number2
    swapHelper.number2 = swapHelper.number1 ^ swapHelper.number2
    swapHelper.number1 = swapHelper.number1 ^ swapHelper.number2

  }

  def main(args: Array[String]): Unit = {

    val startsWithCapitalSample = "Starts with capital"
    val notStartsWithCapitalSample = "starts with capital"
    println(startsWithCapitalSample + " " + startsWithCapital(startsWithCapitalSample))
    println(notStartsWithCapitalSample + " " + startsWithCapital(notStartsWithCapitalSample))

    val numeric = "232329"
    val nonNumeric = "sdsds.assss"
    println("Numeric? " + numeric + " " + isNumeric(numeric))
    println("Numeric? " + nonNumeric + " " + isNumeric(nonNumeric))
    
    val listToReverse = (0 to 10).toList
    println("Original List: " + listToReverse + "Reversed List: " + reverseList(listToReverse))
    
    val swapHelper = new SwapHelper
    swapHelper.number1 = 2
    swapHelper.number2 = 4
    println("Original numbers: " + swapHelper.number1 + "," + swapHelper.number2)
    swapWithoutNewVariable(swapHelper)
    println("Swapped numbers: " + swapHelper.number1 + "," + swapHelper.number2)


  }

}